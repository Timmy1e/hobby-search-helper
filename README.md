# Hobby Search helper
Adds some helpful things to [Hobby Search](https://1999.co.jp/).

Such as:
  - A language switcher
  - A pre-payment order total price 

## Installation
  1. Install Tampermonkey, Greasemonkey, or any other user script Runner.
  2. Go to [here](https://gitlab.com/Timmy1e/hobby-search-helper/raw/master/HobbySearchHelper.user.js), and the script runner should prompt to install it.
  3. Done!

## Development
Don't change the `HobbySearchHelper.user.js` file, it's generated when building.

## Building
Run `./build.sh` from the project root.

## Licence
GNU General Public License v3.0

