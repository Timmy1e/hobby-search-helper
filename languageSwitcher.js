/**
 * @author Tim van Leuverden <TvanLeuverden@Gmail.com>
 * @since 2018-04-17
 * @license GPLv3
 */
/*global $ */

const productRaw = window.location.pathname;
const productJpn = productRaw.match(new RegExp('^(/(.*))?/?$'));
const productEng = productRaw.match(new RegExp('^/eng(/(.*))?$', 'i'));

let productUrl = '';
let productTxt = '';

if (productEng) { // English -> Japanese
	productUrl = '/' + productEng[2] + window.location.search;
	productTxt = '日本に切り替える';
} else {
	if (productJpn) { // Japanese -> English
		productUrl = '/eng/' + productJpn[2] + window.location.search;
		productTxt = 'Switch to English';
	} else { // Neither?
		console.error('Hobby Search switcher:', 'Not english or japanese?');
		// noinspection JSAnnotator
		return;
	}
}

$('.sitemap_h').html('<a href="' + productUrl + '">' + productTxt + '</a>');
// noinspection JSAnnotator
return;
