#!/usr/bin/env bash


out_file='HobbySearchHelper.user.js'

cat user.js > ${out_file}
echo "(()=>{$(cat myPage.js)$(cat languageSwitcher.js)})();" | uglifyjs -cm >> ${out_file}
