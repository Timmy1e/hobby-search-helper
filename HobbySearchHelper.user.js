// ==UserScript==
// @name		Hobby Search helper
// @namespace	https://kiririn.io/
// @version		1.0.0
// @description	Provides helpers for Hobby Search Japan (1999.co.jp)
// @author		Tim van Leuverden <TvanLeuverden@Gmail.com>
// @licence		GPLv3
// @match		http*://www.1999.co.jp/*
// @grant		none
// @updateURL	https://gitlab.com/Timmy1e/hobby-search-helper/raw/master/HobbySearchHelper.user.js
// @downloadURL	https://gitlab.com/Timmy1e/hobby-search-helper/raw/master/HobbySearchHelper.user.js
// ==/UserScript==
(()=>{if("/mypage/"===window.location.pathname||"/eng/mypage/"===window.location.pathname){$("body").append("<style>div.totalPriceBlock {\n\tbackground-color: #F7FBFF;\n\tborder: 1px silver solid;\n\tborder-top: none;\n\tpadding: 5px;\n}</style>");let e="",t="";"/mypage/"===window.location.pathname?(e="注文総額",t="見積もり、送料は含まれていません"):(e="Total price of order",t="Estimated, postage not included");for(let a=0;!($("#header_ilMyPageList_lvOrder_tblItem_"+a).length<1);++a){let l,o=0;for(l=0;;++l){const e=$("#header_ilMyPageList_lvOrder_lvItemDetail_"+a+"_tblItemDetail_"+l+" > tbody > tr > td:nth-child(5)");if(!e||e.length<1)break;o+=parseInt(e.text().replace(" ","").replace("¥","").replace(",",""))}$(`#header_ilMyPageList_lvOrder_lvItemDetail_${a}_tblItemDetail_${l-1}`).after(`<div class="totalPriceBlock" title="${t}">\n\t${e}: <b>&yen;${o}</b>\n</div>`)}return}const e=window.location.pathname,t=e.match(new RegExp("^(/(.*))?/?$")),a=e.match(new RegExp("^/eng(/(.*))?$","i"));let l="",o="";if(a)l="/"+a[2]+window.location.search,o="日本に切り替える";else{if(!t)return void console.error("Hobby Search switcher:","Not english or japanese?");l="/eng/"+t[2]+window.location.search,o="Switch to English"}$(".sitemap_h").html('<a href="'+l+'">'+o+"</a>")})();
