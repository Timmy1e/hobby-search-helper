// ==UserScript==
// @name		Hobby Search helper
// @namespace	https://kiririn.io/
// @version		1.0.0
// @description	Provides helpers for Hobby Search Japan (1999.co.jp)
// @author		Tim van Leuverden <TvanLeuverden@Gmail.com>
// @licence		GPLv3
// @match		http*://www.1999.co.jp/*
// @grant		none
// @updateURL	https://gitlab.com/Timmy1e/hobby-search-helper/raw/master/HobbySearchHelper.user.js
// @downloadURL	https://gitlab.com/Timmy1e/hobby-search-helper/raw/master/HobbySearchHelper.user.js
// ==/UserScript==
