/**
 * @author Tim van Leuverden <TvanLeuverden@Gmail.com>
 * @since 2018-11-29
 * @license GPLv3
 */
/*global $ */

if (window.location.pathname === '/mypage/' || window.location.pathname === '/eng/mypage/') {
	$('body').append(
`<style>div.totalPriceBlock {
	background-color: #F7FBFF;
	border: 1px silver solid;
	border-top: none;
	padding: 5px;
}</style>`
	);
	let totalPriceText = '';
	let titleText = '';
	if (window.location.pathname === '/mypage/') {
		// Japanese
		totalPriceText = '注文総額';
		titleText = '見積もり、送料は含まれていません';
	} else {
		// English
		totalPriceText = 'Total price of order';
		titleText = 'Estimated, postage not included';
	}


	for (let order = 0; true; ++order) {
		if ($('#header_ilMyPageList_lvOrder_tblItem_' + order).length < 1) {
			// No more orders.
			break;
		}

		let item;
		let totalPrice = 0;

		for (item = 0; true; ++item) {
			const itemElm = $('#header_ilMyPageList_lvOrder_lvItemDetail_' + order + '_tblItemDetail_' + item + ' > tbody > tr > td:nth-child(5)');

			if (!itemElm || itemElm.length < 1) {
				// No more items.
				break;
			}

			// Get the price of the item(s) and add it to the total price for the order.
			totalPrice += parseInt(itemElm.text().replace(' ', '').replace('¥', '').replace(',', ''));
		}

		// Append a row with total price to the end of the order tables.
		$(`#header_ilMyPageList_lvOrder_lvItemDetail_${order}_tblItemDetail_${item - 1}`).after(
`<div class="totalPriceBlock" title="${titleText}">
	${totalPriceText}: <b>&yen;${totalPrice}</b>
</div>`
		);
	}

	// noinspection JSAnnotator
	return;
}